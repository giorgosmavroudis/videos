import axios from 'axios';

const KEY = 'AIzaSyDt5k8DUuf35lSqN8uj_JlJc3DYy4nY6AA';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part:'snippet',
        maxResults: 5,
        key: KEY
    }
});