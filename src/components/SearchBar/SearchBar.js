import React, { Component } from 'react';
import './SearchBar.css';

class SearchBar extends Component {
    state ={
        searchField:''
    }

    onSearchChange = (event)=>{
        this.setState({searchField: event.target.value})
    }

    onFormSubmit = event =>{
        event.preventDefault();

        this.props.onSearchSubmit(this.state.searchField)
    }

    render() {
        return (
        <div className='search-bar ui segment'>
            <form onSubmit = {this.onFormSubmit} className='ui form'>
                <div className='field'>
                    <label>Video Search</label>
                    <input 
                        type='text' 
                        value ={this.state.searchField}
                        onChange={this.onSearchChange}/>
                </div>
            </form>
        </div>
        );
    }
}

export default SearchBar;
