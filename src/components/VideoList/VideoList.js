import React from 'react';
import VideoItem from '../VideoItem/VideoItem';

const VideoList = ({ videos, onVideoSelect, selectedVideo }) =>{
    
    const renderedList = videos.map( (video) =>{
        if(selectedVideo !== video){
            return <VideoItem onVideoSelect={onVideoSelect} key={video.id.videoId} video={video}/>
        }
        return null
    })
    return( 
        <div className="ui relaxed divided list">
            {renderedList}
        </div>
    )
}

export default VideoList;