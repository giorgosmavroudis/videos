import React, { Component } from 'react';
import './App.css';
import SearchBar from '../components/SearchBar/SearchBar';
import Youtube from '../apis/Youtube';
import VideoList from '../components/VideoList/VideoList';
import VideoDetail from '../components/VideoDetail/VideoDetail';

class App extends Component {

  state = {
    videos : [],
    selectedVideo: null
  }

  componentDidMount(){
    this.onSearchSubmit('sports cars');
  }

  onSearchSubmit = async term =>{
    const response = await Youtube.get('/search', {
      params:{
        q: term
      }
    })
    this.setState({ 
      videos: response.data.items,
      selectedVideo: null  
    })   
  }

  onVideoSelect = (video) =>{
    this.setState({selectedVideo:video})
  }

  render() {
    return (
      <div className='ui container'>
        <SearchBar onSearchSubmit = {this.onSearchSubmit}/>
        {!this.state.selectedVideo?
          <div className="five wide column">
              <VideoList onVideoSelect={this.onVideoSelect} videos={this.state.videos} selectedVideo={this.state.selectedVideo}/>
          </div>
        :
          <div className="ui grid">
            <div className="ui row">
              <div className="eleven wide column">
                <VideoDetail video={this.state.selectedVideo}/>
              </div>
              <div className="five wide column">
                <VideoList onVideoSelect={this.onVideoSelect} videos={this.state.videos} selectedVideo={this.state.selectedVideo}/>
              </div>
            </div>
          </div>
        }
        
     </div>
    );
  }
}

export default App;
